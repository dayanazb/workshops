var request = new XMLHttpRequest();
var div = document.getElementById('root');

var img = document.getElementById('img');

function myFunction() {
    request.open('GET', 'https://dog.ceo/api/breeds/list/all', true);
    request.onload = function() {

        var data = JSON.parse(this.response);


        Object.keys(data.message).forEach(function(key) {
            div.innerHTML += '<strong>' + key + '</strong> <br>';
        });
    }

    request.send();

}

function random() {
    request.open('GET', 'https://dog.ceo/api/breeds/image/random', true);
    request.onload = function() {

        var data = JSON.parse(this.response);
        img.innerHTML += '<img src= "' + data.message + '"' + 'style="width:280px;height:300px;" >';
    }

    request.send();

}

function breeds() {
    var idbreed = document.getElementById("idbreed");
    var pro = idbreed.options[idbreed.selectedIndex].value;

    request.open('GET', 'https://dog.ceo/api/breed/' + pro + '/images/random', true);
    request.onload = function() {

        var data = JSON.parse(this.response);
        console.log(data);
        img.innerHTML += '<img src= "' + data.message + '"' + 'style="width:280px;height:300px;" >';
    }

    request.send();
}