<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Estudiante;

class EstudianteController extends Controller
{

    public function index(){
        return Estudiante::all();
    }

    public function create(request $request){

        $estudiante = new Estudiante;
        $estudiante->nombre = $request->nombre;
        $estudiante->apellido = $request->apellido;
        $estudiante->telefono = $request->telefono;
        $estudiante->direccion = $request->direccion;
        $estudiante->save();
        return "Los datos se guardaron exitosamente";
    }

    public function update(request $request, $id){

        
        $nombre = $request->nombre;
        $apellido = $request->apellido;
        $telefono = $request->telefono;
        $direccion = $request->direccion;

        $estudiante = Estudiante::find($id);
        $estudiante->nombre = $nombre;
        $estudiante->apellido = $apellido;
        $estudiante->telefono = $telefono;
        $estudiante->direccion = $direccion;
        $estudiante->save();

        return "Los datos se guardaron exitosamente";
    }

    public function delete($id){
        $estudiante = Estudiante::find($id);
        $estudiante->delete();
    }






    
}
